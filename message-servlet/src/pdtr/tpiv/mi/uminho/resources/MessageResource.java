package pdtr.tpiv.mi.uminho.resources;

import pdtr.tpiv.mi.uminho.contract.ActionResult;
import pdtr.tpiv.mi.uminho.contract.Digest;
import pdtr.tpiv.mi.uminho.contract.MessageResult;
import pdtr.tpiv.mi.uminho.contract.Result;
import pdtr.tpiv.mi.uminho.contract.client.Message;
import pdtr.tpiv.mi.uminho.contract.client.MessageService;
import pdtr.tpiv.mi.uminho.contract.client.MessageServiceService;

import javax.print.attribute.standard.Media;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/04/2011
 * Time: 16:27
 * To change this template use File | Settings | File Templates.
 */

@Path("/users/{token}/messages")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class MessageResource {
    private final MessageServiceService service;
    private final MessageService port;
    @Context
    private HttpServletRequest request;

    public MessageResource() {
        service = new MessageServiceService();
        port = service.getMessageServicePort();
    }

    @GET
    public MessageResult getMessages(@PathParam("token") String token) {
        List<Message> messages = port.getMessages(token);
        return new MessageResult(Result.SUCCESS, messages);
    }

    @POST
    public ActionResult postMessages(@PathParam("token") String token, @FormParam("message") String message) {
        port.postMessage(token, message);
        return new ActionResult(Result.SUCCESS);
    }
}
