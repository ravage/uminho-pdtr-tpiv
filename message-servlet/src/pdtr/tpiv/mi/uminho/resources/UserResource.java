package pdtr.tpiv.mi.uminho.resources;

import pdtr.tpiv.mi.uminho.contract.*;
import pdtr.tpiv.mi.uminho.contract.client.MessageService;
import pdtr.tpiv.mi.uminho.contract.client.MessageServiceService;
import pdtr.tpiv.mi.uminho.contract.client.User;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/04/2011
 * Time: 16:26
 * To change this template use File | Settings | File Templates.
 */

@Path("/users")
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public class UserResource {
    private final Digest digest;
    private final MessageServiceService service;
    private final MessageService port;
    @Context
    private HttpServletRequest request;

    public UserResource() {
        digest = Digest.INSTANCE;
        service = new MessageServiceService();
        port = service.getMessageServicePort();
    }

    @POST @Path("/login")
    public AuthenticationResult login(@FormParam("login") String login, @FormParam("password") String password) {
        String hash = digest.toSHA1(password);
        User response = port.login(login, hash, request.getRemoteAddr());

        if (response == null)
            return new AuthenticationResult(Result.ERROR, null);

        return new AuthenticationResult(Result.SUCCESS, response);
    }

    @POST @Path("/register")
    public AuthenticationResult register(@FormParam("login") String login, @FormParam("password") String password) {
        String hash = digest.toSHA1(password);
        User response = port.register(login, hash, request.getRemoteAddr());

        if (response == null)
            return new AuthenticationResult(Result.ERROR, null);


        return new AuthenticationResult(Result.SUCCESS, response);
    }

    @GET @Path("/{token}/is-following-me/{nickname}")
    public ActionResult isFollowingMe(@PathParam("token") String token, @PathParam("nickname") String nickname) {
        String result = port.isFollowingMe(token, new User(-1, nickname)) ? Result.YES : Result.NO;
        return new ActionResult(result);
    }

    @PUT @Path("/{token}/follow")
    public ActionResult follow(@PathParam("token") String token, @FormParam("nickname") String nickname) {
        String result = port.follow(token,  new User(-1, nickname)) ? Result.SUCCESS : Result.ERROR;
        return new ActionResult(result);
    }

    @DELETE @Path("/{token}/unfollow")
    public ActionResult unfollow(@PathParam("token") String token, @FormParam("nickname") String nickname) {
        String result = port.unfollow(token,  new User(-1, nickname)) ? Result.SUCCESS : Result.ERROR;
        return new ActionResult(result);
    }

    @GET @Path("/{token}/followers")
    public UserResult followers(@PathParam("token") String token) {
        List<User> followers = port.getFollowers(token);
        return new UserResult(Result.SUCCESS, followers);
    }

    @GET @Path("/{token}/following")
    public UserResult following(@PathParam("token") String token) {
        List<User> following = port.getFollowing(token);
        return new UserResult(Result.SUCCESS, following);
    }

    @GET @Path("/{token}/others")
    public UserResult others(@PathParam("token") String token) {
        List<User> others = port.getOthers(token);
        return new UserResult(Result.SUCCESS, others);
    }

    @GET @Path("/{token}/notifications")
    public NotificationResult notifications(@PathParam("token") String token) {
        List<String> notifications = port.notifications(token);
        return new NotificationResult(Result.SUCCESS, notifications);

    }
}