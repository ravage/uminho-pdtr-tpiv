package pdtr.tpiv.mi.uminho.servlets;

import com.sun.jersey.api.core.PackagesResourceConfig;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/04/2011
 * Time: 16:18
 * To change this template use File | Settings | File Templates.
 */
@ApplicationPath("/")
public class App extends PackagesResourceConfig {
    public App() {
        super("pdtr.tpiv.mi.uminho.resources");
    }

    @Override
    public Map<String, MediaType> getMediaTypeMappings() {
        Map<String, MediaType> mediaTypes = new HashMap<String, MediaType>();
	    mediaTypes.put("json", MediaType.APPLICATION_JSON_TYPE);
	    mediaTypes.put("xml", MediaType.APPLICATION_XML_TYPE);
        return mediaTypes;
    }
}
