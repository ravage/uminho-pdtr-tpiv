package pdtr.tpiv.mi.uminho.contract;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 31/03/2011
 * Time: 00:05
 * To change this template use File | Settings | File Templates.
 */
public enum Digest {
    INSTANCE;

    public String toSHA1(String text) {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        assert md != null;
        md.update(text.getBytes());
        return new BigInteger(1, md.digest()).toString(16);
    }
}
