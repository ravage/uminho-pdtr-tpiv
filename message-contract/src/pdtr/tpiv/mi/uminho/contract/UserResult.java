package pdtr.tpiv.mi.uminho.contract;

import pdtr.tpiv.mi.uminho.contract.client.User;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 05/05/2011
 * Time: 17:23
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "UserResult")
public class UserResult extends Result {
    @XmlElementWrapper
    private List<User> users;


    public UserResult() {}

    public UserResult(String status, List<User> users) {
        super(status);
        this.users = users;
    }

    public List<User> getUsers() {
        return users;
    }
}
