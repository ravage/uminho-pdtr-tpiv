package pdtr.tpiv.mi.uminho.contract;

import pdtr.tpiv.mi.uminho.contract.Result;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 03/05/2011
 * Time: 17:06
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "ActionResult")
public class ActionResult extends Result {

    public ActionResult() {}

    public ActionResult(String status) {
        super(status);
    }
}
