package pdtr.tpiv.mi.uminho.contract;

import pdtr.tpiv.mi.uminho.contract.client.User;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 29/04/2011
 * Time: 17:20
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement
public class ServiceResponse {
    @XmlAttribute
    private String status;
    @XmlAttribute
    private User message;

    public ServiceResponse() {

    }

    public ServiceResponse(String status, User message) {
        this.status = status;
        this.message = message;
    }
}
