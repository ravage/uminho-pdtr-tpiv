package pdtr.tpiv.mi.uminho.contract;

import pdtr.tpiv.mi.uminho.contract.client.Message;

import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 03/05/2011
 * Time: 17:27
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "MessageResult")
public class MessageResult extends Result {
    @XmlElementWrapper
    private List<Message> messages;

    public MessageResult() {
    }

    public MessageResult(String status, List<Message> messages) {
        super(status);
        this.messages = messages;
    }

    public List<Message> getMessages() {
        return messages;
    }
}
