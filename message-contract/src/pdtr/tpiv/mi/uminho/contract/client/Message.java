package pdtr.tpiv.mi.uminho.contract.client;

import javax.xml.bind.annotation.*;

/**
 * Created by IntelliJ IDEA.
 * pdtr.tpiv.mi.uminho.contract.User: ravage
 * Date: 01/04/2011
 * Time: 18:54
 * To change this template use File | Settings | File Templates.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Message", propOrder = {
    "nickname",
    "message"
})
@XmlRootElement(name = "Message")
public class Message {
    @XmlAttribute(name = "nickname")
    protected String nickname;
    @XmlAttribute(name = "message")
    protected String message;

    public Message() {}

    public Message(String nickname, String message) {
        this.nickname = nickname;
        this.message = message;
    }

     /**
     * Gets the value of the nickname property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getNickname() {
        return nickname;
    }

    /**
     * Sets the value of the nickname property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setNickname(String value) {
        this.nickname = value;
    }

    /**
     * Gets the value of the message property.
     *
     * @return
     *     possible object is
     *     {@link String }
     *
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     *
     * @param value
     *     allowed object is
     *     {@link String }
     *
     */
    public void setMessage(String value) {
        this.message = value;
    }
}
