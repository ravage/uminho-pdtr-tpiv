package pdtr.tpiv.mi.uminho.contract;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 08/05/2011
 * Time: 23:05
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "NotificationResult")
public class NotificationResult extends Result {
    @XmlElementWrapper
    private List<String> notifications;

    public  NotificationResult() {}

    public NotificationResult(String status, List<String> notifications) {
        super(status);
        this.notifications = notifications;
    }

    public List<String> getNotifications() {
        return notifications;
    }
}
