package pdtr.tpiv.mi.uminho.contract;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 01/05/2011
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement
public abstract class Result {
    public final static String ERROR = "result:error";
    public final static String SUCCESS = "result:success";
    public final static String YES = "result:yes";
    public final static String NO  = "result:no";
    public final static String FOLLOW = "result:follow";
    public final static String UNFOLLOW = "result:unfollow";
    public final static String MESSAGES = "result:messages";
    private String status;

    public Result() {}
    public Result(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
