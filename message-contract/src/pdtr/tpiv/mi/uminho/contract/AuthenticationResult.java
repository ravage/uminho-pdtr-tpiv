package pdtr.tpiv.mi.uminho.contract;

import pdtr.tpiv.mi.uminho.contract.client.User;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 01/05/2011
 * Time: 22:15
 * To change this template use File | Settings | File Templates.
 */
@XmlRootElement(name = "AuthenticationResult")
public class AuthenticationResult extends Result {
    private User result;

    public AuthenticationResult() {}

    public AuthenticationResult(String status, User result) {
        super(status);
        this.result = result;
    }

    public User getResult() {
        return result;
    }

    public void setResult(User result) {
        this.result = result;
    }
}
