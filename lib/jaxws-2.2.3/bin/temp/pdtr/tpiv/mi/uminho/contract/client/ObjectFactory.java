
package pdtr.tpiv.mi.uminho.contract.client;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the pdtr.tpiv.mi.uminho.contract.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _IsFollowingMeResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "isFollowingMeResponse");
    private final static QName _GetMessages_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getMessages");
    private final static QName _GetFollowersResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getFollowersResponse");
    private final static QName _GetFollowing_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getFollowing");
    private final static QName _FollowResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "followResponse");
    private final static QName _GetFollowers_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getFollowers");
    private final static QName _LoginResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "loginResponse");
    private final static QName _Message_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "Message");
    private final static QName _User_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "User");
    private final static QName _NotificationsResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "notificationsResponse");
    private final static QName _Unfollow_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "unfollow");
    private final static QName _RegisterResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "registerResponse");
    private final static QName _Follow_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "follow");
    private final static QName _Login_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "login");
    private final static QName _GetFollowingResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getFollowingResponse");
    private final static QName _Notifications_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "notifications");
    private final static QName _GetOthersResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getOthersResponse");
    private final static QName _IsFollowingMe_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "isFollowingMe");
    private final static QName _Register_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "register");
    private final static QName _PostMessage_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "postMessage");
    private final static QName _PostMessageResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "postMessageResponse");
    private final static QName _UnfollowResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "unfollowResponse");
    private final static QName _GetMessagesResponse_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getMessagesResponse");
    private final static QName _GetOthers_QNAME = new QName("http://service.uminho.mi.tpiv.pdtr/", "getOthers");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: pdtr.tpiv.mi.uminho.contract.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link NotificationsResponse }
     * 
     */
    public NotificationsResponse createNotificationsResponse() {
        return new NotificationsResponse();
    }

    /**
     * Create an instance of {@link Unfollow }
     * 
     */
    public Unfollow createUnfollow() {
        return new Unfollow();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link Message }
     * 
     */
    public Message createMessage() {
        return new Message();
    }

    /**
     * Create an instance of {@link GetFollowing }
     * 
     */
    public GetFollowing createGetFollowing() {
        return new GetFollowing();
    }

    /**
     * Create an instance of {@link FollowResponse }
     * 
     */
    public FollowResponse createFollowResponse() {
        return new FollowResponse();
    }

    /**
     * Create an instance of {@link GetFollowers }
     * 
     */
    public GetFollowers createGetFollowers() {
        return new GetFollowers();
    }

    /**
     * Create an instance of {@link IsFollowingMeResponse }
     * 
     */
    public IsFollowingMeResponse createIsFollowingMeResponse() {
        return new IsFollowingMeResponse();
    }

    /**
     * Create an instance of {@link GetMessages }
     * 
     */
    public GetMessages createGetMessages() {
        return new GetMessages();
    }

    /**
     * Create an instance of {@link GetFollowersResponse }
     * 
     */
    public GetFollowersResponse createGetFollowersResponse() {
        return new GetFollowersResponse();
    }

    /**
     * Create an instance of {@link Register }
     * 
     */
    public Register createRegister() {
        return new Register();
    }

    /**
     * Create an instance of {@link UnfollowResponse }
     * 
     */
    public UnfollowResponse createUnfollowResponse() {
        return new UnfollowResponse();
    }

    /**
     * Create an instance of {@link PostMessageResponse }
     * 
     */
    public PostMessageResponse createPostMessageResponse() {
        return new PostMessageResponse();
    }

    /**
     * Create an instance of {@link PostMessage }
     * 
     */
    public PostMessage createPostMessage() {
        return new PostMessage();
    }

    /**
     * Create an instance of {@link GetMessagesResponse }
     * 
     */
    public GetMessagesResponse createGetMessagesResponse() {
        return new GetMessagesResponse();
    }

    /**
     * Create an instance of {@link GetOthers }
     * 
     */
    public GetOthers createGetOthers() {
        return new GetOthers();
    }

    /**
     * Create an instance of {@link Notifications }
     * 
     */
    public Notifications createNotifications() {
        return new Notifications();
    }

    /**
     * Create an instance of {@link GetOthersResponse }
     * 
     */
    public GetOthersResponse createGetOthersResponse() {
        return new GetOthersResponse();
    }

    /**
     * Create an instance of {@link IsFollowingMe }
     * 
     */
    public IsFollowingMe createIsFollowingMe() {
        return new IsFollowingMe();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link GetFollowingResponse }
     * 
     */
    public GetFollowingResponse createGetFollowingResponse() {
        return new GetFollowingResponse();
    }

    /**
     * Create an instance of {@link RegisterResponse }
     * 
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link Follow }
     * 
     */
    public Follow createFollow() {
        return new Follow();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsFollowingMeResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "isFollowingMeResponse")
    public JAXBElement<IsFollowingMeResponse> createIsFollowingMeResponse(IsFollowingMeResponse value) {
        return new JAXBElement<IsFollowingMeResponse>(_IsFollowingMeResponse_QNAME, IsFollowingMeResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMessages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getMessages")
    public JAXBElement<GetMessages> createGetMessages(GetMessages value) {
        return new JAXBElement<GetMessages>(_GetMessages_QNAME, GetMessages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFollowersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getFollowersResponse")
    public JAXBElement<GetFollowersResponse> createGetFollowersResponse(GetFollowersResponse value) {
        return new JAXBElement<GetFollowersResponse>(_GetFollowersResponse_QNAME, GetFollowersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFollowing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getFollowing")
    public JAXBElement<GetFollowing> createGetFollowing(GetFollowing value) {
        return new JAXBElement<GetFollowing>(_GetFollowing_QNAME, GetFollowing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FollowResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "followResponse")
    public JAXBElement<FollowResponse> createFollowResponse(FollowResponse value) {
        return new JAXBElement<FollowResponse>(_FollowResponse_QNAME, FollowResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFollowers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getFollowers")
    public JAXBElement<GetFollowers> createGetFollowers(GetFollowers value) {
        return new JAXBElement<GetFollowers>(_GetFollowers_QNAME, GetFollowers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "loginResponse")
    public JAXBElement<LoginResponse> createLoginResponse(LoginResponse value) {
        return new JAXBElement<LoginResponse>(_LoginResponse_QNAME, LoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Message }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "Message")
    public JAXBElement<Message> createMessage(Message value) {
        return new JAXBElement<Message>(_Message_QNAME, Message.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link User }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "User")
    public JAXBElement<User> createUser(User value) {
        return new JAXBElement<User>(_User_QNAME, User.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotificationsResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "notificationsResponse")
    public JAXBElement<NotificationsResponse> createNotificationsResponse(NotificationsResponse value) {
        return new JAXBElement<NotificationsResponse>(_NotificationsResponse_QNAME, NotificationsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Unfollow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "unfollow")
    public JAXBElement<Unfollow> createUnfollow(Unfollow value) {
        return new JAXBElement<Unfollow>(_Unfollow_QNAME, Unfollow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegisterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "registerResponse")
    public JAXBElement<RegisterResponse> createRegisterResponse(RegisterResponse value) {
        return new JAXBElement<RegisterResponse>(_RegisterResponse_QNAME, RegisterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Follow }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "follow")
    public JAXBElement<Follow> createFollow(Follow value) {
        return new JAXBElement<Follow>(_Follow_QNAME, Follow.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Login }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "login")
    public JAXBElement<Login> createLogin(Login value) {
        return new JAXBElement<Login>(_Login_QNAME, Login.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetFollowingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getFollowingResponse")
    public JAXBElement<GetFollowingResponse> createGetFollowingResponse(GetFollowingResponse value) {
        return new JAXBElement<GetFollowingResponse>(_GetFollowingResponse_QNAME, GetFollowingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Notifications }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "notifications")
    public JAXBElement<Notifications> createNotifications(Notifications value) {
        return new JAXBElement<Notifications>(_Notifications_QNAME, Notifications.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOthersResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getOthersResponse")
    public JAXBElement<GetOthersResponse> createGetOthersResponse(GetOthersResponse value) {
        return new JAXBElement<GetOthersResponse>(_GetOthersResponse_QNAME, GetOthersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsFollowingMe }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "isFollowingMe")
    public JAXBElement<IsFollowingMe> createIsFollowingMe(IsFollowingMe value) {
        return new JAXBElement<IsFollowingMe>(_IsFollowingMe_QNAME, IsFollowingMe.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Register }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "register")
    public JAXBElement<Register> createRegister(Register value) {
        return new JAXBElement<Register>(_Register_QNAME, Register.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostMessage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "postMessage")
    public JAXBElement<PostMessage> createPostMessage(PostMessage value) {
        return new JAXBElement<PostMessage>(_PostMessage_QNAME, PostMessage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PostMessageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "postMessageResponse")
    public JAXBElement<PostMessageResponse> createPostMessageResponse(PostMessageResponse value) {
        return new JAXBElement<PostMessageResponse>(_PostMessageResponse_QNAME, PostMessageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnfollowResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "unfollowResponse")
    public JAXBElement<UnfollowResponse> createUnfollowResponse(UnfollowResponse value) {
        return new JAXBElement<UnfollowResponse>(_UnfollowResponse_QNAME, UnfollowResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetMessagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getMessagesResponse")
    public JAXBElement<GetMessagesResponse> createGetMessagesResponse(GetMessagesResponse value) {
        return new JAXBElement<GetMessagesResponse>(_GetMessagesResponse_QNAME, GetMessagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetOthers }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.uminho.mi.tpiv.pdtr/", name = "getOthers")
    public JAXBElement<GetOthers> createGetOthers(GetOthers value) {
        return new JAXBElement<GetOthers>(_GetOthers_QNAME, GetOthers.class, null, value);
    }

}
