package pdtr.tpiv.mi.uminho.applet;

import javax.swing.*;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 28/03/2011
 * Time: 20:39
 * To change this template use File | Settings | File Templates.
 */
public class App extends JApplet {
    @Override
    public void init() {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
        }
        catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        MessageClientForm mcf = null;
        try {
            mcf = new MessageClientForm();
            mcf.run();
            getContentPane().add(mcf.getContentPane());
            setSize(mcf.getSize());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
/*        try {
            SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    MessageClientForm mcf = null;
                    try {
                        mcf = new MessageClientForm();
                        mcf.run();
                        setSize(mcf.getSize());
                        getContentPane().add(mcf.getContentPane());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (NotBoundException e) {
                        e.printStackTrace();
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                }
            });
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } */
    }
}
