package pdtr.tpiv.mi.uminho.applet;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.representation.Form;
import com.sun.jersey.client.apache.ApacheHttpClient;
import org.omg.CORBA.ORBPackage.InvalidName;
import org.omg.CosNaming.NamingContextPackage.CannotProceed;
import org.omg.CosNaming.NamingContextPackage.NotFound;
import pdtr.tpiv.mi.uminho.contract.*;
import pdtr.tpiv.mi.uminho.contract.client.Message;
import pdtr.tpiv.mi.uminho.contract.client.User;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.awt.*;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Observable;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 28/03/2011
 * Time: 20:38
 * To change this template use File | Settings | File Templates.
 */
public class MessageClient extends Observable implements IMessageServerProxy {
    private String url;
    private final static String BASE_URL = "http://localhost:8080";
    private Client client;
    private WebResource userResource;
    private User currentUser;
    private IClientListener clientListener;

    private final Timer timer;

    public MessageClient(String url) {
        /*if (System.getSecurityManager() == null)
            System.setSecurityManager(new SecurityManager());*/

        client = ApacheHttpClient.create();
        userResource = client.resource(BASE_URL + "/servlets/users");
        this.url = url;
        clientListener = new ClientListener();
        timer = new Timer();
    }

    public boolean register(String username, String password) {
        MultivaluedMap<String, String> params = new Form();
        params.add("login", username);
        params.add("password", password);
        AuthenticationResult result = userResource.path("/register").post(AuthenticationResult.class, params);
        currentUser = result.getResult();
        boolean status = result.getStatus().equals(AuthenticationResult.SUCCESS);

        if (status)
            timer.schedule(new Pooler(), 5000, 5000);

        return status;
    }

    public boolean login(String username, String password) {
        MultivaluedMap<String, String> params = new Form();
        params.add("login", username);
        params.add("password", password);
        AuthenticationResult result = userResource.path("/login").post(AuthenticationResult.class, params);
        currentUser = result.getResult();
        boolean status = result.getStatus().equals(AuthenticationResult.SUCCESS);

        if (status)
            timer.schedule(new Pooler(), 5000, 5000);

        return status;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isFollowingMe(User user) {
        String uri = String.format("%s/is-following-me", currentUser.getToken());
        ActionResult result = userResource.path(currentUser.getToken())
                .path("is-following-me").path(user.getNickname()).get(ActionResult.class);
        return result.getStatus().equals(Result.YES);
    }

    @Override
    public void postMessage(String message) {
        MultivaluedMap<String, String> params = new Form();
        params.add("message", message);
        ActionResult result = userResource.path(currentUser.getToken())
                .path("messages").post(ActionResult.class, params);
    }

    @Override
    public Message[] getMessages() {
        MessageResult result = userResource.path(currentUser.getToken())
                .path("messages").get(MessageResult.class);

        List<Message> messages = result.getMessages();
        return  messages.toArray(new Message[messages.size()]);
    }

    @Override
    public boolean follow(User user) {
        MultivaluedMap<String, String> params = new Form();
        params.add("nickname", user.getNickname());

        ActionResult result = userResource.path(currentUser.getToken())
                .path("follow").put(ActionResult.class, params);

        return result.getStatus().equals(Result.SUCCESS);
    }

    @Override
    public boolean unfollow(User user) {
        MultivaluedMap<String, String> params = new Form();
        params.add("nickname", user.getNickname());

        ActionResult result = userResource.path(currentUser.getToken())
                .path("unfollow").delete(ActionResult.class, params);

        return result.getStatus().equals(Result.SUCCESS);
    }

    @Override
    public User[] getFollowers() {
        UserResult result = userResource.path(currentUser.getToken()).path("followers").get(UserResult.class);
        List<User> followers = result.getUsers();
        return followers.toArray(new User[followers.size()]);
    }

    @Override
    public User[] getFollowing() {
        UserResult result = userResource.path(currentUser.getToken()).path("following").get(UserResult.class);
        List<User> following = result.getUsers();
        return following.toArray(new User[following.size()]);
    }

    @Override
    public User[] getOthers() {
        UserResult result = userResource.path(currentUser.getToken()).path("others").get(UserResult.class);
        List<User> others = result.getUsers();
        return others.toArray(new User[others.size()]);
    }

    private class ClientListener implements IClientListener {
        @Override
        public synchronized void follow(String nickname) {
            setChanged();
            notifyObservers(new RemoteStateChanged<String>(ClientListenerState.FOLLOW, nickname));
        }

        @Override
        public synchronized void unfollow(String nickname) {
            setChanged();
            notifyObservers(new RemoteStateChanged<String>(ClientListenerState.UNFOLLOW, nickname));
        }

        @Override
        public synchronized void messages() {
            setChanged();
            notifyObservers(new RemoteStateChanged<String>(ClientListenerState.MESSAGE, null));
        }
    }

    private class Pooler extends TimerTask {
        @Override
        public void run() {
            NotificationResult result =  userResource.path(currentUser.getToken())
                    .path("notifications").get(NotificationResult.class);

            for (String notification : result.getNotifications()) {
                if (notification.equals(Result.MESSAGES))
                    clientListener.messages();
                else if (notification.equals(Result.FOLLOW))
                    clientListener.follow("Someone");
                else if (notification.equals(Result.UNFOLLOW))
                    clientListener.unfollow("Someone");
            }
        }
    }
}
