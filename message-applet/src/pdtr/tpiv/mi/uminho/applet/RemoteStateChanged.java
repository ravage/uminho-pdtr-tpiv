package pdtr.tpiv.mi.uminho.applet;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 03/04/2011
 * Time: 00:00
 * To change this template use File | Settings | File Templates.
 */
public class RemoteStateChanged<T> {
    private final T data;
    private final ClientListenerState state;

    public RemoteStateChanged(ClientListenerState state, T data) {
        this.state = state;
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public ClientListenerState getState() {
        return state;
    }
}
