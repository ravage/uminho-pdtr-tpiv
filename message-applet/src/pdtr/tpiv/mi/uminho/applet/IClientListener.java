package pdtr.tpiv.mi.uminho.applet;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 02/04/2011
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
public interface IClientListener {
    void follow(String nickname);
    void unfollow(String nickname);
    void messages();
}
