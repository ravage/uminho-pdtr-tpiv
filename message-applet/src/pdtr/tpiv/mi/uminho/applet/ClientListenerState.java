package pdtr.tpiv.mi.uminho.applet;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 02/04/2011
 * Time: 23:45
 * To change this template use File | Settings | File Templates.
 */
public enum ClientListenerState {
    FOLLOW,
    UNFOLLOW,
    MESSAGE
}
