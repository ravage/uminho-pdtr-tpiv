package pdtr.tpiv.mi.uminho.applet;

import pdtr.tpiv.mi.uminho.contract.client.Message;
import pdtr.tpiv.mi.uminho.contract.client.User;

import java.io.Serializable;
import java.rmi.RemoteException;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 15:27
 * To change this template use File | Settings | File Templates.
 */
public interface IMessageServerProxy extends Serializable {
    // store a message on the server
    public void postMessage(String message) throws RemoteException;
    // get the messages stored for the evocating user
    public Message[] getMessages() throws RemoteException;
    // follow a given user
    public boolean follow(User user) throws RemoteException;
    // unfollow  a given user
    public boolean unfollow(User user) throws RemoteException;
    // get followers
    public User[] getFollowers() throws RemoteException;
    // get following
    public User[] getFollowing() throws RemoteException;
    // get all others
    public User[] getOthers() throws RemoteException;
    boolean isFollowingMe(User user) throws RemoteException;
}
