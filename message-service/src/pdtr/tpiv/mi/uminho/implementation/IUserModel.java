package pdtr.tpiv.mi.uminho.implementation;

import pdtr.tpiv.mi.uminho.contract.client.Message;
import pdtr.tpiv.mi.uminho.contract.client.User;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
public interface IUserModel {
    boolean register(String nickname, String password, String token);

    boolean login(String nickname, String password, String token);

    boolean unfollow(User issuer, User user);

    boolean follow(User issuer, User user);

    User[] getFollowing(User issuer);

    User[] getFollowers(User issuer);

    User[] getOthers(User issuer);

    Message[] getMessages(User issuer);

    void post(User issuer, String message);

    int getOid(String nickname);

    boolean isFollowingMe(User issuer, User user);

    List<String> getConnectedFollowers(User issuer);

    String getToken(String name);
}
