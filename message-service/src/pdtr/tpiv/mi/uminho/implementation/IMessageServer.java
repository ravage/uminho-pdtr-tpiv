package pdtr.tpiv.mi.uminho.implementation;

import pdtr.tpiv.mi.uminho.contract.client.Message;
import pdtr.tpiv.mi.uminho.contract.client.User;

/**
 * Created by IntelliJ IDEA.
 * User: ravage
 * Date: 30/03/2011
 * Time: 12:17
 * To change this template use File | Settings | File Templates.
 */
public interface IMessageServer {
    // store a message on the server
    public void postMessage(String token, String message);
    // get the messages stored for the evocating user
    public Message[] getMessages(String token);
    // follow a given user
    public boolean follow(String token, User user);
    // unfollow  a given user
    public boolean unfollow(String token, User user);
    // register the user
    public User register(String nickname, String password, String host);
    // user login
    public User login(String nickname, String password, String host);
    // get followers
    public User[] getFollowers(String token);
    // get following
    public User[] getFollowing(String token);
    // get all others
    public User[] getOthers(String token);
    boolean isFollowingMe(String token, User user);
}
