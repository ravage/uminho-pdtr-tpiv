package pdtr.tpiv.mi.uminho.implementation;

import pdtr.tpiv.mi.uminho.contract.client.User;

import java.util.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by IntelliJ IDEA.
 * pdtr.tpiv.mi.uminho.contract.User: ravage
 * Date: 29/03/2011
 * Time: 18:11
 * To change this template use File | Settings | File Templates.
 */
public enum SessionManager {
    INSTANCE;

    private final ConcurrentHashMap<String, User> session = new ConcurrentHashMap<String, User>();
    private final ConcurrentHashMap<String, Date> times = new ConcurrentHashMap<String, Date>();
    private final ConcurrentHashMap<String, List<String>> notifications = new ConcurrentHashMap<String, List<String>>();

    public void registerSession(User user) {
        session.putIfAbsent(user.getToken(), user);
    }

    public String getToken() {
        return UUID.randomUUID().toString();
    }

    public User getUser(String token) {
        return session.get(token);
    }

    /*pdtr.tpiv.mi.uminho.contract.User getUser(int id) {
        for (pdtr.tpiv.mi.uminho.contract.User user : session.values())
            if (user.getId() == id)
                return user;

        return null;
    }*/


    public synchronized List<String> getNotifications(String token) {
        List<String> notifications = this.notifications.get(token);
        List<String> checkedNotifications = new ArrayList<String>();

        if (notifications != null) {
            for (String s : notifications)
                checkedNotifications.add(s);

            notifications.clear();
        }

        return checkedNotifications;
    }

    public synchronized void addNotification(String token, String notification) {
        List<String> notifications = this.notifications.get(token);

        if (notifications == null) {
            notifications = new ArrayList<String>();
            this.notifications.put(token, notifications);
        }

        if (!notifications.contains(notification))
            notifications.add(notification);
    }

    public void isRegistered(String token) throws SecurityException {
        if (!session.containsKey(token))
            throw new SecurityException("Session expired!");
    }

    public void checkIn(String token) {
        times.put(token, new Date());
        purge();
    }

    private void purge() {
        Iterator<Map.Entry<String, Date>> iterator = times.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Date> item = iterator.next();

            if (item.getValue().getTime() + 20 * 1000 > new Date().getTime())
                continue;

            session.remove(item.getKey());
            iterator.remove();
        }
    }
}
