package pdtr.tpiv.mi.uminho.service;

import com.almworks.sqlite4java.SQLiteException;
import pdtr.tpiv.mi.uminho.contract.Result;
import pdtr.tpiv.mi.uminho.contract.client.Message;
import pdtr.tpiv.mi.uminho.contract.client.User;
import pdtr.tpiv.mi.uminho.implementation.IMessageServer;
import pdtr.tpiv.mi.uminho.implementation.SessionManager;
import pdtr.tpiv.mi.uminho.implementation.UserModel;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * pdtr.tpiv.mi.uminho.contract.User: ravage
 * Date: 28/03/2011
 * Time: 19:27
 * To change this template use File | Settings | File Templates.
 */
@WebService()
public class MessageService implements IMessageServer {
    private final SessionManager session;
    private UserModel model;

    public MessageService() {
        session = SessionManager.INSTANCE;
        try {
            model = new UserModel();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    @WebMethod
    public void postMessage(@WebParam(name = "token") final String token, @WebParam(name = "message") final String message) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);

        List<String> followers = model.getConnectedFollowers(issuer);
        for (String userToken : followers) {
            User user = session.getUser(userToken);
            if (user != null) {
                session.addNotification(user.getToken(), Result.MESSAGES);
            }
        }

        model.post(issuer, message);
    }

    @WebMethod
    public Message[] getMessages(@WebParam(name = "token") String token) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);

        return model.getMessages(issuer);
    }

    @WebMethod
    public boolean follow(@WebParam(name = "token") String token, @WebParam(name = "user") final User user) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);
        boolean followResult = model.follow(issuer, user);

        session.addNotification(model.getToken(user.getNickname()), Result.FOLLOW);

        return followResult && followResult;
    }

    @WebMethod
    public boolean unfollow(@WebParam(name = "token") String token, @WebParam(name = "user") final User user) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);

        session.addNotification(model.getToken(user.getNickname()), Result.UNFOLLOW);

        return model.unfollow(issuer, user);
    }

    @WebMethod
    public User register(@WebParam(name = "nickname") final String nickname, @WebParam(name = "password") String password, @WebParam(name = "host") final String host) {
        final String token = session.getToken();
        final boolean state = model.register(nickname, password, token);
        final User issuer = new User(nickname, token, host);

        if (state)
            session.registerSession(issuer);
        else
            return null;

        issuer.setOid(model.getOid(issuer.getNickname()));

        return new User(issuer.getOid(), issuer.getNickname(), issuer.getToken());
    }

    @WebMethod
    public User login(@WebParam(name = "nickname") final String nickname, @WebParam(name = "password") final String password, @WebParam(name = "host") final String host) {
        final String token = session.getToken();
        final boolean loginResult = model.login(nickname, password, token);

        if (!loginResult)
            return null;

        final User issuer = new User(nickname, token, host);
        issuer.setOid(model.getOid(nickname));

        session.registerSession(issuer);

        return new User(issuer.getOid(), issuer.getNickname(), issuer.getToken());
    }

    @WebMethod
    public User[] getFollowers(@WebParam(name = "token") String token) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);

        return model.getFollowers(issuer);
    }

    @WebMethod
    public User[] getFollowing(@WebParam(name = "token") String token) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);

        return model.getFollowing(issuer);
    }

    @WebMethod
    public User[] getOthers(@WebParam(name = "token") String token) {
        session.isRegistered(token);
        final User issuer = session.getUser(token);

        return model.getOthers(issuer);
    }

    @WebMethod
    public boolean isFollowingMe(@WebParam(name = "token") String token, @WebParam(name = "user") User user) {
        session.isRegistered(token);
        final User requester = session.getUser(token);
        return model.isFollowingMe(requester, user);
    }

    @WebMethod
    public String[] notifications(@WebParam(name = "token") String token) {
        session.isRegistered(token);
        List<String> notificationsList = session.getNotifications(token);
        return notificationsList.toArray(new String[notificationsList.size()]);
    }
}