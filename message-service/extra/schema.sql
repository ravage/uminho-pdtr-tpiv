CREATE TABLE users (
    id INTEGER PRIMARY KEY,
    nickname VARCHAR(128) NOT NULL,
    password VARCHAR(512) NOT NULL,
    connected BIT DEFAULT 0,
    token VARCHAR(256),
    last_read_request DATETIME DEFAULT CURRENT_TIMESTAMP,
    inserted_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME
);

CREATE TABLE messages (
    id INTEGER PRIMARY KEY,
    user_id INTEGER NOT NULL,
    message VARCHAR(1024) NOT NULL,
    inserted_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME,
    FOREIGN KEY (user_id) REFERENCES users (id)
);



CREATE TABLE connections (
    id INTEGER PRIMARY KEY,
    user_id INTEGER NOT NULL,
    other_id INTEGER NOT NULL,
    inserted_at DATETIME DEFAULT CURRENT_TIMESTAMP,
    updated_at DATETIME,
    FOREIGN KEY (user_id) REFERENCES users (id),
    FOREIGN KEY (other_id) REFERENCES users (id)
);

CREATE INDEX messages_user_idx ON messages (user_id);
CREATE UNIQUE INDEX connections_users_idx ON connections (user_id, other_id);
CREATE UNIQUE INDEX users_nickname_idx on users (nickname);
